import { useState } from "react"
import Header from "./components/Header"
import Tasks from "./components/Tasks"

const App = () => {
  const [tasks, setTasks] = useState([])
  const addTask = (data) => {
    setTasks([
      ...tasks,
      {
        id: crypto.randomUUID(),
        isCompleted: false,
        title: data
      }
    ])
  }

  const toggleTaskComplete = (taskId) => {
    const newTask = tasks.map(task => {
      if (task.id === taskId) {
        return {
          ...task,
          isCompleted: !task.isCompleted
        }
      }
      return task
    })
    setTasks(newTask)
  }

  const toggleTaskDelete = (taskId) => {
    const newTask = tasks.filter(task => task.id !== taskId)
    setTasks(newTask)
  }


  return (
    <>
      <Header onAddTask={addTask} />
      <Tasks  
        tasks={tasks} 
        onComplete={toggleTaskComplete}  
        onDelete={toggleTaskDelete}
      />
    </>
  )
}

export default App
