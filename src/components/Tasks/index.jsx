import '../../assets/styles/styles.css'
import Task from '../task'

const Tasks = ({ tasks, onComplete, onDelete }) => {
  const taskQuantity = tasks.length
  const taskCompleted = tasks.filter(task => task.isCompleted).length
  return (
    <section className='task_container'>
      <header className='task_header'>
        <div>
          <p>Create tasks</p>
          <span>{taskQuantity}</span>
        </div>

        <div>
          <p>Completed</p>
          <span>{taskCompleted} of {taskQuantity}</span>
        </div>
      </header>

      <div className="list_task">
        {
          tasks.map(task => (
            <Task key={task.id} task={task} onComplete={onComplete} onDelete={onDelete}/>
          ))
        }
      </div>
    </section>
  )
}

export default Tasks