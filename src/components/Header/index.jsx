import '../../assets/styles/styles.css'
import { AiOutlinePlusCircle } from 'react-icons/ai'
import { useState } from 'react'

const Header = ({ onAddTask }) => {
  const [title, setTitle] = useState('')
  const handleSubmit = (e) => {
    e.preventDefault()
    onAddTask(title)
    setTitle('')
  }

  const handleOnChange = (e) => {
    setTitle(e.target.value)
  }
  return (
    <header className="header">
      <h1>Header</h1>
  
      <form onSubmit={handleSubmit} className='task'>
        <input 
          className='form-field' 
          type="text" 
          placeholder='add a new task' 
          value={title} 
          onChange={handleOnChange}
        />
        <button>
          Create
          <AiOutlinePlusCircle />
        </button>
      </form>
    </header>
  )
}

export default Header